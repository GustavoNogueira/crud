<?php

namespace App\Http\Requests\Painel;

use Illuminate\Foundation\Http\FormRequest;

class ClienteFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'name' => 'required|min:3|max:100', //preenchimento obrigatorio no minimo 3 caracteres. maximo 100
            'email' => 'required', //
           
    ];

    return [
        'name.required' => 'O campo nome é obrigatorio!',
        'email.required' => 'Insira seu endereço de email!',
       

            ];
    }
}
