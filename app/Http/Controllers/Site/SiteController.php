<?php

namespace App\Http\Controllers\Site; # mudar namespace para nova pasta sites

use Illuminate\Http\Request;
use App\Http\Controllers\controller;   # add caminho para classe controllers
/*
class SiteController extends Controller
{

    public function __construct()
    {
        #$this-> middleware('auth');
        #$this->middleware('auth')->only(['categoria']); #especificando quais metodos passarão pelo middleware

        #$this->middleware('auth')->except(['index','contato']); #indica quem nao passa pelo middleware
        
    
    }

    public function index()
    {
        
        $title = 'Titulo teste';
        $xss = '<script>alert("Ataque xss");</script>'; #alert() alerta
        $var1 = '123';
        $arrayData = [1,2,3,4,5];

        #return view('index',['teste'=>$teste]); #Passar variavel para view
        return view('site.home.index',compact('title','xss','var1','arrayData')); #Passar variavel para view de forma compacta 
        #O site.home.index quer dizer que a view index está dentro da pasta site em views
    }

    public function contato()
    {
        return view('site.contact.index');
    }

    public function categoria($id)
    {
        return "Categoria {$id}";
    }

    public function categoriaOp($id = 1)
    {
        return "Categoria {$id}";
    }
}*/
