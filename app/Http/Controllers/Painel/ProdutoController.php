<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Painel\Product;
use App\Http\Requests\Painel\ProductFormRequest;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $product;
    private $totalPage = 10;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        $title = 'Listagem produtos';
        $products = $this->product->paginate($this->totalPage);
        return view('painel.products.index',compact('products','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Cadastro';

        $categorys = ['eletronicos','moveis','limpeza','banho'];

        return view('painel.products.create-edit', compact('title','categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductFormRequest $request) #automaticamente cria um objeto da classe request
    {
        $dataForm = $request->all();


        $insert = $this->product->create($dataForm); //Faz cadastro

        if($insert)
        {
            return redirect()->route('produtos.index'); 
        }
        else 
            return redirect()->route('produtos.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->find($id);

        $title = "Produto: $product->name";

        return view('painel.products.show',compact('product','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $product = $this->product->find($id); //recuperando produto pelo id

        $title = "Editar Produto: $product->name";
        
        $categorys = ['eletronicos','moveis','limpeza','banho'];

        return view('painel.products.create-edit', compact('title','categorys','product'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductFormRequest $request, $id)
    {
        //editar item

        //Recupera todos os itens
        $dataForm = $request->all();

        //recupera o item pelo id
        $product = $this->product->find($id);

        //verifica se ativo
        $dataForm['active'] = ( !isset($dataForm['active'])) ? 0: 1 ;

        //altera os itens
        $update = $product->update($dataForm);

            //verifica se foi alterado
            if($update)
            {
                return redirect()->route('produtos.index');
            }
            else 
                return redirect()->route('produtos.edit',$id)->with(['errors'=>'Falha ao editar']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->product->find($id);

        $delete =  $product->delete();

        if($delete)
        {
            return redirect()->route('produtos.index');
        }
        else return redirect()->route('produtos.show', $id)->with(['errors'=>'Falha ao deletar']);
    }

    public function tests()
    {
       /*$insert = $this->product->create([
            'name'          => 'Amaciante',
            'number'        => 458,
            'active'        =>false,
            'category'      =>'limpeza',
            'description'   =>'Descrição',

       ]);

       
        if($insert){
            return "Inserido com sucesso id: {$insert ->id}";
        }
        else
            return 'Falha de inserção';*/ //Funçao de inserção no banco de dados

            
            /*
            $prod = $this->product->find(5);

            //dd($prod -> name); //debuga a variavel

            $update = $prod ->update([
                'name'          => 'update Teste 2',
                'number'        => 110,
                'active'        =>true,
             
            ]);

            if($update)
            {
                return 'Alterado com sucesso';
            }
            else return 'Falha ao alterar'; */ //Update em registro do banco

            /*
            $update = $this->product
            ->where('number',147852963)
            ->update([
                    'name'          =>'update numero',
                    'number'        =>4547,
                    'active'        =>true,
             
                ]);

            if($update)
            {
                return 'Alterado com sucesso 2';
            }
            else return 'Falha ao alterar'; */ //Update pela coluna numero

            $prod = $this->product->find(3);
            $delete = $prod->delete(); //ou destroy(id,id,id); sem usar o find

            if($delete)
            {
                return 'Exlusão realizada com sucesso';

            }
            else 
                return 'Falha ao excluir';

    }
}
