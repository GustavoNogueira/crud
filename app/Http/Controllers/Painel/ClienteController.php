<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Painel\Cliente;
use App\Http\Requests\Painel\ClienteFormRequest;


class ClienteController extends Controller
{

    private $cliente;
    private $totalPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct(Cliente $cliente)
     {
         $this->cliente = $cliente;
     }

    public function index()
    {   
        $title = 'Listagem produtos';
        $clientes = $this->cliente->paginate($this->totalPage);
        return view('Painel.clientes.index',compact('clientes','title'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('painel.clientes.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClienteFormRequest $request)
    {
        $dataForm = $request->all();
       
        $insert = $this->cliente->create($dataForm); //Faz cadastro

        if($insert)
        {
            return redirect()->route('clientes.index'); 
        }
        else 
            return redirect()->route('clientes.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
                $cliente = $this->cliente->find($id);
        
                $title = "Produto: $cliente->name";
        
                return view('painel.clientes.show',compact('cliente','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                $cliente = $this->cliente->find($id); //recuperando produto pelo id
        
                $title = "Editar Cliente: $cliente->name";
                        
                return view('painel.clientes.create-edit', compact('title','cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataForm = $request->all();
        
                //recupera o item pelo id
                $cliente = $this->cliente->find($id);
        
                //altera os itens
                $update = $cliente->update($dataForm);
        
                    //verifica se foi alterado
                    if($update)
                    {
                        return redirect()->route('clientes.index');
                    }
                    else 
                        return redirect()->route('clientes.edit',$id)->with(['errors'=>'Falha ao editar']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $cliente = $this->cliente->find($id);
        
            $delete =  $cliente->delete();
        
            if($delete)
            {
                 return redirect()->route('clientes.index');
            }
                 else return redirect()->route('clientes.show', $id)->with(['errors'=>'Falha ao deletar']);
    }
}
