<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Painel\Pedido;
use App\Models\Painel\Cliente;
use App\Models\Painel\Product;
use App\Models\Painel\Carrinho;
use App\Http\Requests\Painel\PedidoFormRequest;
use App\Http\Requests\Painel\ProductFormRequest;


class PedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $pedido;
    
    
    private $totalPage = 10;

    public function __construct(Pedido $pedido)
    {
        
        $this->pedido = $pedido;
        
    
    }

    public function index()
    {
        $title = 'Listagem pedidos';
        $pedidos = $this->pedido->with('cliente')->paginate($this->totalPage);
        
        return view('Painel.pedidos.index',compact('pedidos','title'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Cadastro';

        $plucked = cliente::pluck('name','id');

        $produtos = product::pluck('name','id','preco'); 
        
        /*dd($plucked->all());
        $plucked1->all();*/
        
        return view('painel.pedidos.create-edit',compact('title','plucked','produtos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PedidoFormRequest $request)
    {
        $dataForm = $request->all();   
        
        //dd($dataForm);

        $produto_id = $dataForm['produto_id']; //ids dos produtos 

        /*$pd_id = (int)$produto_id;
        $teste = product::where('id', $pd_id)->get();

        dd($teste);*/
         
        $insert = $this->pedido->create($dataForm); //Faz cadastro

        $pedido_id = $insert->id;
        
        
        $quantidade = $dataForm['Quantidade'];
        $sub_Total = $dataForm['SubTotal'];

        //$pl=['produto_id' => $teste, 'pedido_id' => '1' , 'produto' => 'teste', 'quantidade' => '1','sub_total' => '1'];
        

    

        $contador = count(request('produto_id'));
        $produto_id = request('produto_id');
        $quantidade = request('Quantidade');
        $sub_total = request('SubTotal');
        $pedido_id = $insert->id;
        

        for($x=0; $x<$contador; $x++){
        $data[] = [
            'produto_id' => (int)$produto_id[$x],
            'pedido_id' => $pedido_id,
            'quantidade' => $quantidade[$x],
            'sub_total' => $sub_total[$x]        
        ];
        carrinho::create($data[$x]);
    } 


          
      
        //dd($insert->id); //id do pedido

        if($insert)
        {           
          
            return redirect()->route('pedidos.index'); 
        }   
        
        else 
            return redirect()->route('pedidos.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
                $pedido = $this->pedido->find($id);
                $carrinhos = carrinho::where('pedido_id' , $id)->get();
                $total =$carrinhos->sum('sub_total');
                
               # $title = "Pedido: $pedido->name";
        
                return view('painel.pedidos.show',compact('pedido','carrinhos','total'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $pedido = $this->pedido->find($id); //recuperando produto pelo id

        return view('painel.pedidos.create-edit', compact('pedido'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $requests , $id)
    {
         //editar item

        //Recupera todos os itens
                $dataForm = $request->all();
        
                //recupera o item pelo id
                $pedido = $this->pedido->find($id);

                $idProduto = $dataForm['produto_id']; //Capturando o id do produto cadastrado
                
                $buscaId = $requests->find($idProduto); //buca o produto pelo id
                
                $buscaPreco = $buscaId['preco']; //busca o preço setado no produto selecionado
                        
                $dataForm['total'] = $buscaPreco * $dataForm['quantidade']; 
        
                //altera os itens
                $update = $pedido->update($dataForm);
        
                    //verifica se foi alterado
                    if($update)
                    {
                        return redirect()->route('pedidos.index');
                    }
                    else 
                        return redirect()->route('pedidos.edit',$id)->with(['errors'=>'Falha ao editar']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
                $pedido = $this->pedido->find($id);
        
                $delete =  $pedido->delete();
        
                if($delete)
                {
                    return redirect()->route('pedidos.index');
                }
                else return redirect()->route('pedidos.show', $id)->with(['errors'=>'Falha ao deletar']);
    }

    public function teste(Pedido $requestss)
    {   
                dd($dataForm = $request->all());        

                $pedido = $this->pedido->find($id);
        
                return redirect()->route('pedidos.index');
    }

}
