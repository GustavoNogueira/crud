<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Painel\Carrinho;
use App\Models\Painel\Product;
use App\Models\Painel\Pedido;

class CarrinhoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $carrinho;
    private $totalPage = 10;

    public function __construct(Carrinho $carrinho)
    {
        $this->carrinho = $carrinho;
    }

    public function index($idPedido)
    {
        dd($idPedido);
        $title = 'Listagem pedidos';
        $carrinhos = $this->carrinho->with('product')->paginate($this->totalPage);
        return view('painel.carrinhos.index',compact('carrinhos','title','idPedido')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {   
        
        $pedidoId = $id;

        $plucked = product::pluck('name','id','preco');

        return view('painel.carrinhos.create-edit',compact('pedidoId','plucked'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request ,Product $requestss)
    {

        $dataForm = $request->all();
        
        dd($dataForm);

        $idProduto = $dataForm['produto_id'];

        $buscaProduto = $requestss->find($idProduto);

        $buscaPreco = $buscaProduto['preco']; //busca o preco setado no produto selecionado

        $subTotal = $buscaPreco * $dataForm['produto_qtde'];

        $dataForm['sub_total'] = $subTotal;

        $buscaNome = $buscaProduto['name'];

        $dataForm['produto'] = $buscaNome;

        $insert = $this->carrinho->create($dataForm); //Faz cadastro
        
        $idCarrinho = $insert['id'];

        $pedido_id = Carrinho::find($idCarrinho)->only('pedido_id');
                
        $carrinhos = carrinho::where('pedido_id',$pedido_id)->get();

        if($insert)
        {
            return view('painel.pedidos.show',compact('carrinhos'));
        }
        else 
            return redirect()->route('carrinho.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $pedido_id = Carrinho::find($id)->only('pedido_id');

        $teste = Carrinho::find($id);
        
        $delete =  $teste->delete();
        
        $carrinhos = carrinho::where('pedido_id',$pedido_id)->get();
        
        
            if($delete)
                {
                     return view('painel.pedidos.show', compact('carrinhos'));
                }
                     else return redirect()->route('carrinho.show', $id)->with(['errors'=>'Falha ao deletar']);
    }
}
