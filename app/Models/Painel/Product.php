<?php

namespace App\Models\Painel;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';  
    protected $fillable = ['name','number','active','category','description','preco']; /* quais colunas podem ser modificadas*/
    
    //protected $guarded = []; /*quais colunas não podem ser modificadas*/



    Public function pedido(){
        
        return $this->hasone(Pedido::class);
       }

    Public function carrinho(){
        
        return $this->hasMany(Carrinho::class);
       }
    
}
