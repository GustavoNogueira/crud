<?php

namespace App\Models\Painel;

use Illuminate\Database\Eloquent\Model;

class Carrinho extends Model
{

    protected $fillable = ['produto_id','pedido_id','produto','quantidade','sub_total'];

    public function product()
    {
        return $this->belongsTo(Product::class,'produto_id');
    }

    public function pedido()
    {
        return $this->belongsTo(Pedido::class,'pedido_id');
    }
}
