<?php

namespace App\Models\Painel;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = ['quantidade','total','cliente_id','preco','carrinho_id'];
    
    public function cliente()
    {
        return $this->belongsTo(Cliente::class,'cliente_id');
    }


    public function carrinho()
    {
        return $this->hasMany(Carrinho::class,'carrinho_id');
    }


    
}




