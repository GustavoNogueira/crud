<?php

namespace App\Models\Painel;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['name','email','senha']; 

    Public function pedido(){
        
        return $this->hasone(Pedido::class);
       }
   
}
