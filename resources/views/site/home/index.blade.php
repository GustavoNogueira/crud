@extends('site.template.template1')

@section('content')

<h1>Pagina inicial</h1>
{{$title ?? 'Não existe'}} <!-- ?? Se não existir var1 é recebido o valor default -->

@if($var1 == '1235')
    <p>É igual</p>

@else
    <p>O valor é diferente</p>

@endif

@unless($var1 == 123) <!-- inverso do unless -->
    <p>Não é igual</p>

@endunless

@for($i=0; $i<10; $i++)
    <p>for: {{$i}}</p>
@endfor


@if(count($arrayData > 0))
@foreach($arrayData as $array) 
    <p>foreach: {{$array}}</p>
@endforeach

@else
<p>Nao existem valores</p>

@endif

@forelse($arrayData as $array)  
<p>Forelse: {{$array}}</p>

@empty
<p>nao existe</p>
@endforelse


@include('site.includes.sidebar', compact('var1'))


@endsection

@push('scripts')

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
@endpush

