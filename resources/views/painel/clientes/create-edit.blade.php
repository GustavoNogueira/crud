
@extends('adminlte::page')

@section('content')

<h1 class="title-pg">
    <a href="{{route('clientes.index')}}"><span class="glyphicon glyphicon-fast-backward"></span></a>
    Gestão clientes: <b>{{$cliente->name ?? 'Novo'}}</b>
</h1>

@if(isset($errors) && count($errors)>0)

    <div class= "alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>

@endif

@if(isset($cliente))
            
        {!! Form::model($cliente, ['route'=>['clientes.update', $cliente->id],'class'=>'form', 'method'=>'put']) !!}
        
@else
  
    {!! Form::open(['route'=>'clientes.store', 'class'=>'form']) !!}
        

@endif


    {!!csrf_field() !!} 
    <div class="form-group"> 
    {!! Form::text('name',null, ['class' => 'form-control','placeholder '=> 'Nome:']) !!}
    </div>

    <div class="form-group"> 
        {!! Form::text('email',null, ['class' => 'form-control','placeholder '=> 'Email:']) !!}
        </div>

    {!! Form::submit('Enviar', ['class'=>'btn-primary']) !!}
    
    
    {!! Form::close() !!}
    

@endsection