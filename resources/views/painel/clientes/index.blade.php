@extends('adminlte::page') <!-- Puxando layout do adminLte -->


@section('content')

<h1 class="title-pg">Listagem dos clientes</h1>

<a href="{{route('clientes.create')}}" class="btn btn-primary btn-add"> <!-- route( redireciona ao apertar o botao para a rota indicada) -->
    <span class="glyphicon glyphicon-plus"></span> Cadastrar
</a>

<table class="table table-striped">
    <tr>
        <th>Nome</th>
        <th>Email</th>   
        <th width = "100px">Ações</th>
    </tr>

    @foreach ($clientes as $cliente)

        <tr>
            <td>{{$cliente->name}}</td>
            <td>{{$cliente->email}}</td>
            
            
            <td>
                <a href="{{route('clientes.edit', $cliente->id)}}" class="actions edit"> <!-- Apontando para rota edit -->
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
                
                <a href="{{route('clientes.show', $cliente->id)}}" class="actions delete">
                    <span class="glyphicon glyphicon-eye-open"></span>
                </a>
            </td>    
        </tr>
        
    @endforeach

</table>

{!! $clientes->links()!!}

@endsection
