@extends('adminlte::page')

@section('content')

<h1 class="title-pg">
    <a href="{{route('clientes.index')}}"><span class="glyphicon glyphicon-fast-backward"></span></a>
    Clientes: <b>{{$cliente->name}}</b>
</h1>
<p><b>Nome: </b>{{$cliente->name}}</p>
<p><b>Email: </b>{{$cliente->email}}</p>


<hr>

@if(isset($errors) && count($errors)>0)

    <div class= "alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>

@endif


{!! Form::open(['route'=>['clientes.destroy',$cliente->id],'method'=>'DELETE']) !!}

{!! Form::submit("Deletar Cliente: $cliente->name", ['class' =>'btn btn-danger']) !!}


{!! Form::close() !!}



@endsection