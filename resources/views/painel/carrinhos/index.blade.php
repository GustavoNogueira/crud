@extends('adminlte::page') <!-- Puxando layout do adminLte -->


@section('content')

<h1 class="title-pg">Itens {{$idPedido}}</h1>

<a href="/carrinho_create/{{$idPedido}}" class="btn btn-primary btn-add"> <!-- route( redireciona ao apertar o botao para a rota indicada) -->
    <span class="glyphicon glyphicon-plus"></span> Adicionar
</a>

<a href="{{route('pedidos.store', $idPedido)}}" class="btn btn-primary btn-add"> <!-- route( redireciona ao apertar o botao para a rota indicada) -->
    <span class="glyphicon glyphicon-plus"></span> Finalizar
</a>

<table class="table table-striped">
    <tr>
        <th>Produto</th> 
        <th>Valor</th>
        <th>Quantidade</th>        
        <th width = "100px">Ações</th>
    </tr>


</table>

@endsection
