
@extends('adminlte::page')

@section('content')


<h1 class="title-pg">
    <a href="{{route('pedidos.index')}}"><span class="glyphicon glyphicon-fast-backward"></span></a>
    Gestão itens: <b>{{$pedidoId ?? 'Novo'}}</b>
</h1>

@if(isset($errors) && count($errors)>0)

    <div class= "alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>

@endif

@if(isset($carrinho))
            
        {!! Form::model($carrinho, ['route'=>['carrinho.update', $carrinho->id],'class'=>'form', 'method'=>'put']) !!}
        
@else
  
    {!! Form::open(['route'=>'carrinho.store', 'class'=>'form']) !!}
        
@endif

    {!!csrf_field() !!} 

    <div class="form-group">
        
        {!! Form::select('produto_id', $plucked, null,['class'=>'form-control']) !!}  
    </div>

    <div class="form-group">   
        {!!  Form::text('quantidade',null, ['class' => 'form-control','placeholder '=> 'Quantidade:']) !!}
    </div>

    <div class="form-group">   
            {!!  Form::text('pedido_id', $pedidoId, ['class' => 'form-control','placeholder '=> 'teste:']) !!}
        </div>

    {!! Form::submit('Adicionar', ['class'=>'btn-primary']) !!}
    
    
    {!! Form::close() !!}
    

@endsection