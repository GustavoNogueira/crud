@extends('adminlte::page') <!-- Puxando layout do adminLte -->


@section('content')

<h1 class="title-pg">Listagem de pedidos</h1>


<a href="{{route('pedidos.create')}}" class="btn btn-primary btn-add"> <!-- route( redireciona ao apertar o botao para a rota indicada) -->
    <span class="glyphicon glyphicon-plus"></span> Cadastrar
</a>

<table class="table table-striped">
<thead>
    <tr>
        <th>Cliente</th>
        <th>Email</th>    
        <th width = "100px">Ações</th>
    </tr>
    
</thead>
        <tr>
    @foreach ($pedidos as $pedido)

            <td>{{$pedido->cliente->name}}</td>
            <td>{{$pedido->cliente->email}}</td>
            
            
            <td>
                <a href="{{route('pedidos.edit', $pedido->id)}}" class="actions edit"> <!-- Apontando para rota edit -->
                    <span class="glyphicon glyphicon-pencil"></span>
                </a>
                
                <a href="{{route('pedidos.show', $pedido->id)}}" class="actions delete">
                    <span class="glyphicon glyphicon-eye-open"></span>
                </a>

                {{--<a href="{{route('pedidos.destroy', $pedido->id)}}" class="actions delete">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>--}}
            </td>    
        </tr>

     
    @endforeach

</table>

{!! $pedidos->links()!!}

@endsection
