@extends('adminlte::page')

@php
$total2 =0;
@endphp

@section('content')

<h1 class="title-pg">
    <a href="{{route('pedidos.index')}}"><span class="glyphicon glyphicon-fast-backward"></span></a>
    Itens
</h1>


@if(isset($errors) && count($errors)>0)

    <div class= "alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{$error}}</p>
        @endforeach
    </div>
@endif


<table class="table table-striped">
    <thead>
        <tr>
            <th>Produto</th>  
            <th>Quantidade</th>
            <th>Sub Total</th> 
            <th>Ação</th>  
            <th width = "150px">Total</th>
        </tr>
    </thead> 

    @if(isset($carrinhos)) 
        @foreach ($carrinhos as $carrinho)
            <tr>
                <td>{{$carrinho->product->name}}</td>
                <td>{{$carrinho->quantidade}}</td>
                <td>{{$carrinho->sub_total}}</td>
                
                @php
                $total2 += $carrinho->sub_total
                @endphp
                <td>
                        {{--<a href="{{route('pedidos.index')}}" class="actions edit"> <!-- Apontando para rota edit -->
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a> --}}
                        
                        {!! Form::open(['route'=>['carrinho.destroy',$carrinho->id],'method'=>'DELETE']) !!}
                        
                        {!! Form::submit("Excluir", ['class' =>'btn btn-danger']) !!}
                        
                        
                        {!! Form::close() !!}
                    </td>    
            </tr>
            
        @endforeach 
        <td></td> 
        <td></td> 
        <td></td>
        <td></td>  
        <td><b>{{$total ?? $total2}}</b></td>      
    @endif

@endsection