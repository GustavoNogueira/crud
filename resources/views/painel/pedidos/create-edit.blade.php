
@extends('adminlte::page')

@section('content')


    <div class="row">
        
    
    
    {!! Form::open(['route'=>'pedidos.store', 'class'=>'form']) !!}
    
    <td>{!! Form::select('cliente_id', $plucked, null,['class'=>'form-control']) !!}</td>
    
    
    {!! Form::select('produto_id', $produtos, null,['class'=>'form-control','id'=> 'produto' ]) !!}
    
    {!! Form::number('produto_qtde',  0,['class'=>'form-control','id'=> 'produto_qtde']) !!}
    {!! Form::text('produto_preco', null,['class'=>'form-control','id'=> 'produto_preco']) !!}

    <div class="modal-body">
            <div class="col-sm-6">
                </div>
                <div style="right: 7%" align="right" class="col-sm-6">
                    <a type="button" class="btn btn-success" onclick="adicionaLinha()">Add</a>
                </div>
                {!! Form::submit('Salvar', ['class'=>'btn-primary']) !!}
                {!! Form::close() !!}

        <table  class="table table-striped table-hover dataTables" id="tbgrade" >
            <thead>
            <tr>
                <th align="center">Produto</th>
                <th align="center">Quantidade</th>
                <th align="center">Preço Unitario</th>
                <th align="center">Sub Total</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                    <tr class="item">
     
    
                    </tr>      
            </tbody>
        </table>
       
    </div>
    
</div>

@endsection


<script>
             //Funcao para adicionar uma nova linha na tabela
             function adicionaLinha() {
                var newRow = $("<tr>");
                var Qtd = $("#produto_qtde").val();
                var Unitario = $("#produto_preco").val();
                var SubT = Qtd*Unitario;
                var cols = 
                
                '<td contenteditable="true"><input type="text" class="disabled no-border hidden" name="produto_id[]" value="' + $("#produto").val() + '"/>' + $("#produto option:selected").text() + '</td>' +
                '<td contenteditable="true"><input type="text" class="disabled no-border" name="Quantidade[]" value="' + $("#produto_qtde").val() + '"/></td>' +
                '<td contenteditable="true"><input type="text" class="disabled no-border" name="Preco[]" value="' + $("#produto_preco").val() + '"/></td>' +
                '<td contenteditable="true"><input type="text" class="disabled no-border" name="SubTotal[]" value=" ' + SubT + ' "/></td>' +
                '<td><button type="button" class="btn btn-danger" data-delete="endereco" onclick="remove(this)">Excluir</button></td>';
                
                
                newRow.append(cols);
                $('#tbgrade').find('tbody').append(newRow);}
            
</script>
