<?php

Route::get('/painel/produtos/tests','Painel\ProdutoController@tests'); 
Route::resource('painel/produtos', 'Painel\ProdutoController');
Route::resource('painel/clientes', 'Painel\ClienteController'); 
Route::resource('painel/pedidos', 'Painel\PedidoController');
Route::resource('painel/carrinho', 'Painel\CarrinhoController');

Route::get('/carrinhos/{id}', 'Painel\CarrinhoController@create');



Route::get('/carrinho/{id}', 'Painel\CarrinhoController@index');
Route::get('/carrinho_create/{id}', 'Painel\CarrinhoController@create');
Route::get('/teste/{id}', 'Painel\CarrinhoController@create');

#Route::get('/item/{id}', 'Painel\CarrinhoController@index');



Route::group(['namespace' => 'Site'], function() {#Caminho Site\SiteController #Grupo presente no mesmo namespcace

    Route::get('/categoria/{id}', 'SiteController@categoria');
    Route::get('/categoria2/{id?}', 'SiteController@categoriaOp');
    


    Route::get('/contato','SiteController@contato');
   

});

Route::get('/','HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'HomeController@index')->name('home');

